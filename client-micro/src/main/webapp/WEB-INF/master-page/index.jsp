<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		
		<title>GastNet - Dashboard</title>
		<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/img/AOLogo.png" />
		
		<tiles:insertAttribute name="links" />
	</head>
	
	<body id="page-top">
	
	<%
    	response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
   		response.setHeader("Pragma","no-cache"); //HTTP 1.0
   		response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	%>
	
		<tiles:insertAttribute name="header" />
		
		<br/>
		
		<tiles:insertAttribute name="body" />
		
		<tiles:insertAttribute name="footer" />
	
		<tiles:insertAttribute name="functions"/>
		
	</body>
</html>