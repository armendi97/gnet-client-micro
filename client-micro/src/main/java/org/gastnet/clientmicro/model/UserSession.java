package org.gastnet.clientmicro.model;

import org.gastnet.clientmicro.enumeration.Role;

import lombok.NoArgsConstructor;

import lombok.Data;

@Data
@NoArgsConstructor
public class UserSession {
	
	private long userId;
	private String userEmail;
	private Role role;
	private String token;
	private long businessId;
	
	public UserSession(long userId, String userEmail, Role role, String token) {
		this.userId = userId;
		this.userEmail = userEmail;
		this.role = role;
		this.token = token;
	}
}
