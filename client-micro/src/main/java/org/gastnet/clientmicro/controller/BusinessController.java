package org.gastnet.clientmicro.controller;

import javax.servlet.http.HttpServletRequest;

import org.gastnet.clientmicro.dto.BusinessProfileDTO;
import org.gastnet.clientmicro.enumeration.ContactType;
import org.gastnet.clientmicro.enumeration.URL;
import org.gastnet.clientmicro.model.Contact;
import org.gastnet.clientmicro.model.Individual;
import org.gastnet.clientmicro.model.Location;
import org.gastnet.clientmicro.model.UserSession;
import org.gastnet.clientmicro.service.BusinessService;
import org.gastnet.clientmicro.util.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class BusinessController {

	
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private BusinessService businessService;
	
	@PostMapping("/new-location")
	public String addLocation(@ModelAttribute Location location , HttpServletRequest request) {
		UserSession session = (UserSession)request.getSession().getAttribute("UserSession");
		restTemplate.exchange(RequestUtils.getAuthorizedPostRequest(request,location,
				RequestUtils.getURI(URL.BUSINESS_LOCATION, ""+session.getBusinessId())), Object.class);
		return "redirect:/business-profile" ;
	}
	
	@GetMapping("/business-profile")
	public String business(HttpServletRequest request, Model model) {
		UserSession userSession = (UserSession)request.getSession().getAttribute("UserSession");
		BusinessProfileDTO dto = businessService.getBusinessProfileData(userSession.getUserId(),request);
		userSession.setBusinessId(dto.getBusiness().getBusinessId());
		model.addAttribute("contactTypes", ContactType.values()).addAttribute("newContact", new Contact())
				.addAttribute("newLocation", new Location()).addAttribute("business", dto.getBusiness())
				.addAttribute("contacts", dto.getContacts()).addAttribute("locations", dto.getLocations())
				.addAttribute("expertises", dto.getExpertises());
		request.getSession().setAttribute("UserSession", userSession);
		return "business-profile";
	}
}
